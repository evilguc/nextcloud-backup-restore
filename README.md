# Nextcloud Backup-n-Restore &middot;  [![License: MIT](https://img.shields.io/badge/license-MIT-blue.svg)](./LICENSE)
> Simple Nextcloud Backup and Restore Scripts

Straightforward and opinionated implementation of backup and restore for Nextcloud. Initially created for my Nextcloud instance on Raspberry PI.
For reference read Nextcloud [Backup](https://docs.nextcloud.com/server/stable/admin_manual/maintenance/backup.html) and [Restoring Backup](https://docs.nextcloud.com/server/stable/admin_manual/maintenance/restore.html) documentation pages.

### Backup script usage
```bash
./backup.sh [pi] [/var/www/nextcloud] [/mnt/nextcloud_data] # host, nextcloud-dir and nextcloud-data-dir
```
(works better with `ssh-agent`, credentials will be promted otherwise on each step)

**NOTE**: it expects that MySQL or MariaDB is used as database backend with `nextcloud` database.

As a result 3 files will be created:
* `./nextcloud-dir_YYYYMMDD.tar` - backup of a Nextcloud directory
* `./nextcloud_data-dir_YYYYMMDD.tar` - backup of your files in Nextcloud
* `./nextcloud-mysql_YYYYMMDD.dump` - SQL dump of a Nextcloud database

### Restore script usage

**ALERT: before running this script please make sure you've read it and fully understand what it does.**

This script will:
* delete everything from Nextcloud directory (defaults to `/var/www/nextcloud`)
* delete everything from Nextcloud data directory (defaults to `/mnt/nextcloud_data`)
* drop your Nextcloud database (`nextcloud` database)

```bash
./restore.sh <version> [pi] [/var/www/nextcloud] [/mnt/nextcloud_data] # version is a YYYYMMDD part from ./backup.sh output (like 20190215), assuming corresponding files are in place
```

## Known Issues

* Currently it does not work properly with files stored in `<nextcloud-dir>/data` (it will provide redundant backup and restore step)

## License

[MIT licensed](./LICENSE)
