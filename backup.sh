#! /bin/bash
set -e

DATE=$(date +"%Y%m%d")

SSH_HOST=${1:-"pi"}
NEXTCLOUD_DIR=${2:-"/var/www/nextcloud"}
NEXTCLOUD_DATA_DIR=${3:-"/mnt/nextcloud_data"}

echo "=== enable maintenance mode ==="
ssh "$SSH_HOST" "/bin/sh -c \"sudo -u www-data /bin/sh -c \\\"cd $NEXTCLOUD_DIR && php occ maintenance:mode --on\\\"\""

echo "=== backup nextcloud dir ==="
ssh "$SSH_HOST" "sudo tar -cpf - --one-file-system $NEXTCLOUD_DIR" > nextcloud-dir_$DATE.tar

echo "=== backup nextcloud data dir ==="
ssh "$SSH_HOST" "sudo tar -cpf - --one-file-system $NEXTCLOUD_DATA_DIR" > nextcloud_data-dir_$DATE.tar

echo "=== backup database ==="
ssh "$SSH_HOST" 'sudo mysqldump --single-transaction nextcloud' > nextcloud-mysql_$DATE.dump

echo "=== disable maintenance mode ==="
ssh "$SSH_HOST" "/bin/sh -c \"sudo -u www-data /bin/sh -c \\\"cd $NEXTCLOUD_DIR && php occ maintenance:mode --off\\\"\""

echo "=== calculate checksums ==="
sha256sum nextcloud-dir_$DATE.tar nextcloud_data-dir_$DATE.tar nextcloud-mysql_$DATE.dump > nextcloud-backup_$DATE.sha256
