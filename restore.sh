#! /bin/bash
set -e

DATE=$(date +"%Y%m%d")

VERSION=$1
SSH_HOST=${2:-"pi"}
NEXTCLOUD_DIR=${3:-"/var/www/nextcloud"}
NEXTCLOUD_DATA_DIR=${4:-"/mnt/nextcloud_data"}

echo "=== erase nextcloud dir ==="
echo "NOT IMPLEMENTED YET"

echo "=== erase nextcloud data dir ==="
echo "NOT IMPLEMENTED YET"

echo "=== drop and create empty nextcloud database ==="
ssh "$SSH_HOST" "sudo mysql -e 'DROP DATABASE nextcloud'"
ssh "$SSH_HOST" "sudo mysql -e 'CREATE DATABASE nextcloud CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci'"

echo "=== restore nextcloud database from backup ==="
ssh "$SSH_HOST" "sudo mysql nextcloud" < nextcloud-mysql_$VERSION.dump

echo "=== restore nextcloud data dir from backup ==="
ssh "$SSH_HOST" "sudo tar -C / -xf -" < nextcloud_data-dir_$VERSION.tar

echo "=== restore nextcloud dir from backup ==="
ssh "$SSH_HOST" "sudo tar -C / -xf -" < nextcloud-dir_$VERSION.tar

echo "=== disable maintenance mode ==="
ssh "$SSH_HOST" "/bin/sh -c \"sudo -u www-data /bin/sh -c \\\"cd $NEXTCLOUD_DIR && php occ maintenance:mode --off\\\"\""
